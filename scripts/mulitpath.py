'''Script Intent: Interface with Kleinschmidt's Proprietary Acoustic Telemetry
Data Management and Positioning Software to perform multipath filtering.

Script utilizes multiprocessing, make sure the number of processes is not greater
than the number of cores you have on your computer.

Script Author: KPN'''

# Import Modules:
import os
import sys
import multiprocessing as mp
sys.path.append(r"C:\Users\Kevin Nebiolo\Desktop\Cowlitz_Desktop_091318\Program")
#import telemetryAnalysis_5_deco as telem
import acoustic_positioning_seconds as acoustic
import pandas as pd
import sqlite3
import warnings
import numpy as np
warnings.filterwarnings('ignore')
import time

if __name__ == "__main__":
    ts = time.time()
    outputWS = r"C:\Users\Kevin Nebiolo\Desktop\Cowlitz_Desktop_091318\Output\Scratch"
    inputWS = r"C:\Users\Kevin Nebiolo\Desktop\Cowlitz_Desktop_091318\Data"
    dbName = 'cowlitz_2018.db'
    dbDir = os.path.join(inputWS,dbName)
    #dbDir = r"C:\Users\Kevin Nebiolo\Desktop\cowlitz_tagDrag.db"

    # get list of tags
    conn = sqlite3.connect(dbDir)
    c = conn.cursor()
    tags = pd.read_sql('SELECT Tag_ID FROM tblTag WHERE TagType != "beacon"',con = conn).Tag_ID.values
    tags = np.sort(tags)    
    #tags = ['FF76','FF74','FF78','7DD2','FF75','FF81','FF79','FF80','FF77']
    #tags = ['FF76','FF74','FF78','7DD2','FF75','FF81','FF79','FF80','FF77']
          

    #tags = tags[187:]  
    c.close()
    print "There are %s fish to iterate through"%(len(tags))
    iters = []                                                                 # make an empty list to iterate through
    for i in tags:
        iters.append(acoustic.multipath_data_object(i,dbDir,outputWS))         # for every tag, make a multipath data object and append to iterable list
        print "Fish %s added to list"%(i)
    
    print "All tags added to list, now map the multipath functon and apply over multiple processors"
    print "Depending upon the amount of data, this can take a while, call your mom, read a book, get cultured"
    pool = mp.Pool(processes = 3)
    pool.map(acoustic.multipath_2, iters)
    acoustic.multipath_data_management(outputWS,dbDir,primary = True)
    
    # apply secondary filter
    for i in tags:
        print "Secondary mulitpath filter applied to tag %s"%(i)
        acoustic.mulitpath_classifier(i,dbDir,outputWS,beacon = False) 
    
    acoustic.multipath_data_management(outputWS,dbDir,primary = False)
    
    print "All tags processes, proceed to positioning"
    print "Multipath filtering took %s seconds to compile"%(round(time.time() - ts,4)) 
    del iters    
    
    
'''Script Intent: Interface with Kleinschmidt's Proprietary Acoustic Telemetry
Data Management and Positioning Software to coordinate fish using Deng's exact 
solution.

Script utilizes multiprocessing, make sure the number of processes is not greater
than the number of cores you have on your computer.

Script Author: KPN'''

# Import Modules:
import os
import jsats3d
import warnings
warnings.filterwarnings('ignore')
import time

print ("Modules Imported, set parameters")
ts = time.time()
outputWS = r"D:\Cowlitz\2018"
figureWS = r"D:\Cowlitz\2018"
inputWS = r"D:\Cowlitz\2018"
dbName = 'cowlitz_2018.db'
dbDir = os.path.join(inputWS,dbName)
recList = ['R01','R02','R03','R05','R06','R07','R08','R09']
testTag = 'FF76'

# create a position object for our test tag
pos = jsats3d.position(testTag,recList,dbDir,outputWS,figureWS)
print ("position object created, initialize Deng's solution")
# coordinate using Deng's exact method
pos.Deng()
pos.trajectory_plot_Deng()
